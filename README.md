# Check_mk plugin blacklist

GPL (C) Lars Falk-Petersen <dev@falk-petersen.no>, 2020

https://gitlab01.copyleft.no/sysdevs/checkmk-blacklist or https://gitlab.com/larsfp/checkmk-blacklist

## Overview

Check_mk plugin to check if a host is blacklisted. See https://tools.ietf.org/html/rfc5782 - DNS Blacklists and Whitelists.

* Should be run as a cached plugin, i.e. in local/3600/ to run once an hour.

## Requirements

* Python 3.5 or higher.
  
## Files

* blacklist - run on Check_mk server
* blacklist-agent.py - run on targets

## Example

![alt text](img/example.png "Example of one blacklisting.")

Output from agent:

```bash
<<<blacklist>>>
blacklist_abuse.ch 0 Not listed.
blacklist_anti-spam.cn 0 Not listed.
blacklist_rbl-dns 1 193.xxx.xxx.86 listed in dul.rbl-dns.com https://www.rbl-dns.com/bl?ip=193.xxx.xxx.86 , 
blacklist_status 0 2020-01-21 12:32, running every 3600 second, Agent version 3, IPs checked: {'193.xxx.xxx.86'}. 
```

Output from checkmk:

```bash
blacklist_abuse.ch   OK - Not listed.
blacklist_anti-spam.cn OK - Not listed.
blacklist_barracuda  OK - Not listed.
blacklist_rbl-dns    WARN - 193.xxx.xxx.86 listed in dul.rbl-dns.com https://www.rbl-dns.com/bl?ip=193.xxx.xxx.86 ,
blacklist_status     OK - 2020-01-21 12:32, running every 3600 second, Agent version 3, IPs checked: {'193.xxx.xxx.86'}.
```

## TODO

* ipv6
* More lists?
  * http://multirbl.valli.org/
  * https://www.dnsbl.info/dnsbl-database-check.php
  * https://en.wikipedia.org/wiki/Comparison_of_DNS_blacklists
* Some (all?) can give more verbose output in txt record. Perhaps it should be shown. Example:
  * $ dig txt 86.x.x.193.bl.rbl-dns.com +short
    "Listed in DUL - Dynamic IPs - see https://www.rbl-dns.com/bl?ip=193.x.x.86"

## Inspiration

* check_spamblockv2.py
* https://github.com/whotwagner/check_mk-rbl