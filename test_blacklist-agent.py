# https://docs.python.org/3/library/unittest.html

# Run me this way:
# python -m unittest -v test_blacklist-agent.py

# Requires this symlink:
#   ln -s blacklist-agent.py blacklistagent.py

import unittest
import blacklistagent

class TestBlacklistMethods(unittest.TestCase):

    def test_build_collection(self):
        ''' Check we get a long list of blacklists '''

        collection = blacklistagent.build_collection()
        self.assertGreater(len(collection), 1, msg='Got a blacklist collection less than one.')

    def test_get_host_and_IPs(self):
        ''' Check hostname and IPs discovered '''

        hostname = 'gitlab01.copyleft.no'
        gethostname, getexternal_ips = blacklistagent.get_host_and_IPs(hostname)
        self.assertEqual(hostname, gethostname, msg="Hostname %s doesn't match testcase %s" % (gethostname, hostname))

    def test_blacklists_with_alwaysblack(self):
        '''
        Do a DNS look-up on all blacklists with a specified dummy address 
        which should always return " listed in ..." '''

        collection = blacklistagent.build_collection()
        print('')
        for blacklist in collection:
            if blacklist.always_black:
                with self.subTest(blacklist):
                    result = blacklist.check_list({blacklist.always_black:blacklistagent.reverse_ipv4(blacklist.always_black)})
                    print(result)
                    self.assertIn(' listed ', result, msg='A list returned an error: %s' % result)
                
    def test_blacklists_with_alwayswhite(self):
        '''
        Do a DNS look-up on all blacklists with a specified dummy address 
        which should always return " not listed" '''

        collection = blacklistagent.build_collection()
        print('')
        for blacklist in collection:
            if blacklist.always_white:
                with self.subTest(blacklist):
                    result = blacklist.check_list({blacklist.always_white:blacklistagent.reverse_ipv4(blacklist.always_white)})
                    self.assertLess(len(result), 1, msg='A list returned an error: %s' % result)

    # https://tools.ietf.org/html/rfc5782#section-5
    # IPv4-based DNSxLs MUST contain an entry for 127.0.0.2 for testing
    # purposes.  IPv4-based DNSxLs MUST NOT contain an entry for 127.0.0.1.

    #    IPv6-based DNSxLs MUST contain an entry for ::FFFF:7F00:2 (::FFFF:
    #    127.0.0.2), and MUST NOT contain an entry for ::FFFF:7F00:1 (::FFFF:
    #    127.0.0.1), the IPv4-Mapped IPv6 Address [RFC4291] equivalents of the
    #    IPv4 test addresses.


if __name__ == '__main__':
    unittest.main()